��    X      �     �      �     �     �     �     �  _   �               %     7  
   I     T     ]     i  	   m     w     {     �     �  	   �     �     �     �     �     �     �     �     �     �     �     �  7   �     #	     0	     C	     Z	     c	     g	     n	     r	     x	     |	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	  +   �	     

     
  	   #
     -
     1
  X   9
  (   �
  8   �
     �
  F   �
     D     H  
   L     W     _     c     l     t     �  !   �     �     �     �     �     �     �  R   �     )  [   D  \   �  �  �     �     �     �     �  W   �     N     [     b  
   r     }     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �     �     �     �                
  1        B     N     `     o     w     {     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  	   �     �     �     �  )   �          -     1     :     >  ^   F  (   �  /   �     �  7        E     I  	   M     W     `  	   d     n     s     {     �     �     �     �     �     �     �  R   �  %     #   ;  ]   _     :              %   3   9            6         ;   A   F      <   J          5   ?   0   =               I                    X   S   (   E              .               L   U      2       
              W   &               	   K       T       O      Q       P   1       7      N   C      D      /   >   R   *       M                    +                B   '      )       G   $   ,           V       4   H   @                  !   "                         #   -           8    Abr Abril Ago Agosto App gratuita. Patrocinada por
\Ferrer S.A.
\Publicada por
\ © Licitelco España S.L. ec-europe Borrar evento Cancelar Cardiología 2014 Cerrar formulario Comentario Comienza Contraseña Dic Diciembre Dom Domingo Día Editar Email: %1 Ene Enero Error Escriba aquí sus notas Evento Feb Febrero Guardar Hecho Hora Hoy Imposible leer el idioma predeterminado del dispositivo Información Información Extra Información extra: %1 Ingresar Jue Jueves Jul Julio Jun Junio Lista Lun Lunes Mar Mar* Martes Marzo May Mayo Mes Mie Miércoles No Notas Notas Personales Notas personales sobre: <strong>%1</strong> Notas personales: %1 Nov Noviembre Oct Octubre Para verificar usuario y contraseña debe tener al menos una conexión a internet activa Por favor, ingrese usuario y contraseña Por favor, ingrese usuario y contraseña para continuar. Pregunta Problema de conexión. Por favor, inténtelo de nuevo en unos segundos Sab Sep Septiembre Sábado Sí Tel.: %1 Termina Todo el día Usuario Usuario o contraseña incorrectos Vie Viernes Volver Website: %1 desde hasta http://multimedia.ec-europe.com/es/ferrer/calendario_cardiologia/ficha_tecnica.pdf http://www.ferrergrupo.com ¿Es usted profesional sanitario con capacidad de prescripción o dispensación en España? ¿Seguro que desea borrar sus notas para este evento?
Esta operación no puede ser deshecha. Project-Id-Version: Calendario
POT-Creation-Date: 2014-01-08 17:56+0100
PO-Revision-Date: 2014-01-08 17:56+0100
Last-Translator: Nico Fantino <nfanti@hotmail.com>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.3
X-Poedit-Basepath: ../
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;appGettext.strargs;strargs
X-Poedit-SearchPath-0: js
 Apr April Aug August Free app. Sponsored by
\Ferrer S.A.
\Published by
\ © Licitelco España S.L. ec-europe Delete event Cancel Cardiology 2014 Close form Comment Begins Password Dec December Sun Sunday Day Edit Email: %1 Jan January Error Write your notes here Event Feb February Save Done Time Today Error reading device locale default configuration Information Extra Information Extra Info: %1 Sign In Thu Thursday Jul July Jun June List Mon Monday Tue Mar Tuesday March May May Month Wed Wednesday No Notes Personal Notes Personal notes about: <strong>%1</strong> Personal notes: %1 Nov November Oct October In order to verify username and password you must have at least one active internet connection Please, enter your username and password Please, fill username and password to continue. Please, answer Conectivity problem. Please, try again in a few seconds Sat Sep September Saturday Yes Phone: %1 Ends All day User Wrong username or password Fri Friday Back Website: %1 from until http://multimedia.ec-europe.com/en/ferrer/calendario_cardiologia/ficha_tecnica.pdf http://www.ferrergrupo.com/Inicio-ENG Are you a healthcare professional?	 Are you sure you want to delete your notes about this event?
This operation cannot be undone. 
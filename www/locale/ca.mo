��    X      �     �      �     �     �     �     �  _   �               %     7  
   I     T     ]     i  	   m     w     {     �     �  	   �     �     �     �     �     �     �     �     �     �     �     �  7   �     #	     0	     C	     Z	     c	     g	     n	     r	     x	     |	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	  +   �	     

     
  	   #
     -
     1
  X   9
  (   �
  8   �
     �
  F   �
     D     H  
   L     W     _     c     l     t     �  !   �     �     �     �     �     �     �  R   �     )  [   D  \   �  �  �     �     �     �     �  _   �     V  
   k     v     �  	   �     �     �     �     �     �     �     �     �  	   �     �     �     �     �     	               #     (     ,     1  7   6     n     z     �  
   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                                *   -     X     l     q     z       T   �  3   �  B        S  P   \     �     �     �     �     �     �     �  
   �     �     �       	   
               +     2  R   7  %   �  X   �  c   	     :              %   3   9            6         ;   A   F      <   J          5   ?   0   =               I                    X   S   (   E              .               L   U      2       
              W   &               	   K       T       O      Q       P   1       7      N   C      D      /   >   R   *       M                    +                B   '      )       G   $   ,           V       4   H   @                  !   "                         #   -           8    Abr Abril Ago Agosto App gratuita. Patrocinada por
\Ferrer S.A.
\Publicada por
\ © Licitelco España S.L. ec-europe Borrar evento Cancelar Cardiología 2014 Cerrar formulario Comentario Comienza Contraseña Dic Diciembre Dom Domingo Día Editar Email: %1 Ene Enero Error Escriba aquí sus notas Evento Feb Febrero Guardar Hecho Hora Hoy Imposible leer el idioma predeterminado del dispositivo Información Información Extra Información extra: %1 Ingresar Jue Jueves Jul Julio Jun Junio Lista Lun Lunes Mar Mar* Martes Marzo May Mayo Mes Mie Miércoles No Notas Notas Personales Notas personales sobre: <strong>%1</strong> Notas personales: %1 Nov Noviembre Oct Octubre Para verificar usuario y contraseña debe tener al menos una conexión a internet activa Por favor, ingrese usuario y contraseña Por favor, ingrese usuario y contraseña para continuar. Pregunta Problema de conexión. Por favor, inténtelo de nuevo en unos segundos Sab Sep Septiembre Sábado Sí Tel.: %1 Termina Todo el día Usuario Usuario o contraseña incorrectos Vie Viernes Volver Website: %1 desde hasta http://multimedia.ec-europe.com/es/ferrer/calendario_cardiologia/ficha_tecnica.pdf http://www.ferrergrupo.com ¿Es usted profesional sanitario con capacidad de prescripción o dispensación en España? ¿Seguro que desea borrar sus notas para este evento?
Esta operación no puede ser deshecha. Project-Id-Version: Calendario
POT-Creation-Date: 2014-01-08 17:56+0100
PO-Revision-Date: 2014-05-08 16:26+0100
Last-Translator: Nico Fantino <nfanti@hotmail.com>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.5
X-Poedit-Basepath: ../
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;appGettext.strargs;strargs
X-Poedit-SearchPath-0: js
 abr. abril ag. agost App gratuïta. Patrocinada per
\Ferrer S.A.
\Published by
\ © Licitelco España S.L. ec-europe Esborra esdeveniment Cancel·la Cardiologia 2014 Tanca formulari Comentari Inici Clau des. desembre dg. diumenge Dia Edita Email: %1 gen. gener Error Escriu les teves notes aquí Esdeveniment febr. febrer Desa Fet Hora Avui Impossible llegir l'idioma predeterminat del dispositiu Informació Informació extra Informació extra: %1 Introdueix dj. dijous jul. juliol juny juny Llista dl. dilluns dt. març dimarts març maig maig Mes dc. dimecres No Notes Notes personals Notes personals sobre: <strong>%1</strong> Notes personals: %1 nov. novembre oct. octubre Per tal de verificar l'usuari i la clau has de tenir una connexió a Internet activa Si us plau, introdueix el teu usuari i la teva clau Si us plau introdueix el teu usuari i la teva clau per a continuar Pregunta Hi ha un problema de connexió. Si us plau, torna-ho a provar d'aquí uns segons ds. set. setembre dissabte Sí Tel: %1 Final Tot el dia Usuari Usuari o clau incorrectes dv. divendres Enrere Pàgina Web: %1 Des de fins http://multimedia.ec-europe.com/en/ferrer/calendario_cardiologia/ficha_tecnica.pdf http://www.ferrergrupo.com/Inicio-ENG És vostè professional sanitari amb capacitat de prescripció o dispensació a Espanya? Està segur que vol esborrar les notes per aquest esdeveniment?
Aquesta operació no es pot desfer. 
var db;
var updateurl;
var data = [];

var eventsSync = {

    init: function () {
        updateurl = 'http://calendar.ec-europe.com/?key='+ appSpecific.key;

        if(userType !== false)
            eventsSync.startup();
        else
            setTimeout(eventsSync.init, 100);
    },
    dbError: function (e) {
        console.log("SQL ERROR");
        console.log(e);
    },
    startup: function () {
        db = window.openDatabase(appSpecific.dbName +"-db", appSpecific.dbVersion, appSpecific.client +" "+ appSpecific.appName +" DB", appSpecific.dbSize);
        db.transaction(this.initDB, this.dbError, this.dbReady);
    },
    initDB: function (tx) {
        var installed = window.localStorage.getItem(appSpecific.dbName +"-db") ? window.localStorage.getItem(appSpecific.dbName +"-db") : 'no';
        if (installed === 'yes') {
            //console.log('NOT first run');
            return true;
        } else {
            eventsSync.populateDB(tx);
        }
    },
    dbReady: function () {
        //console.log('dbReady')
        if (conn === true) {
            eventsSync.syncDB();
        } else {
            eventsSync.displayDocs();
        }
    },
    syncDB: function () {
        //console.log('Syncing DB online');
        var date = window.localStorage.getItem("lastdate") && window.localStorage.getItem("lastdate") !== 'undefined' ? window.localStorage.getItem("lastdate") : '1990-01-01';

        $.get(updateurl + '&date=' + date, function (resp) {
              if (resp.length)
            resp.forEach(function (ob) {
                db.transaction(function (ctx) {
                    ctx.executeSql("select id from events where token = ?", [ob.token], function (tx, checkres) {
                        if (checkres.rows.length) {
                            if (!ob.deleted) {
                                //console.log('Updating Item: '+ ob.token);
                                tx.executeSql("update events set start_date=?,end_date=?,text=?,details=?,location=?,website=?,email=?,phone=?,extra=?,color=?,x=?,y=?,lastupdated=? where token=?", [ob.start_date, ob.end_date, ob.text, ob.details, ob.location, ob.website, ob.email, ob.phone, ob.extra, ob.color, ob.x, ob.y, ob.lastupdated, ob.token]);
                            } else {
                                tx.executeSql("delete from events where token = ?", [ob.token]);
                            }
                        } else {
                            if (!ob.deleted) {
                                //console.log('Inserting Item: '+ ob.token);
                                tx.executeSql("insert into events(start_date,end_date,text,details,location,website,email,phone,extra,color,x,y,lastupdated,token) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [ob.start_date, ob.end_date, ob.text, ob.details, ob.location, ob.website, ob.email, ob.phone, ob.extra, ob.color, ob.x, ob.y, ob.lastupdated, ob.token]);
                            }
                        }

                    });
                });
            });

            if (typeof resp[resp.length -1] !== 'undefined'){
                window.localStorage.setItem("lastdate", resp[resp.length -1].lastupdate);
            }

            eventsSync.displayDocs();
        }, "json").fail(function() { eventsSync.displayDocs(); });

    },
    displayDocs: function () {
        //console.log('displayDocs')
        db.transaction(function (tx) {
            tx.executeSql("select * from events order by lastupdated asc", [], function (tx, results) {

                for (var i = 0; i < results.rows.length; i++) {
                    data.push(results.rows.item(i));
                }
                //console.log(data)

                dhxReady();
            });
        });
    },
    populateDB: function (tx){
        //console.log('first run');
        appSpecific.populateDB(tx);
        window.localStorage.setItem(appSpecific.dbName +"-db", 'yes');
    }
};
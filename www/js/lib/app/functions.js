var conn, userType, w, extcal, locale;

function onDeviceReady() {
    navigator.globalization.getLocaleName(
        function(locale) {
            locale = locale.value.split('_')[0];
            locale = locale.split('-')[0];
            jQuery.get('./locale/' + locale + '.po', function() {
                appGettext = new Gettext({
                    'domain': locale
                });
                jQuery.getScript('./js/lib/app/functions.js');
                jQuery.getScript('./js/app-specific.js');
                jQuery.getScript('./js/lib/dhtmlx/locale.js');
                StatusBar.hide();
                App.init();
            }).fail(function() {
                StatusBar.hide();
                App.init();
            });
        },
        function() {
            navigator.notification.alert(__('Imposible leer el idioma predeterminado del dispositivo'), null, __('Error'));
        }
    );
}
var App = {
    init: function() {
        jQuery("body").on("click", "a[rel='external']", function() {
            window.open(encodeURI(jQuery(this).attr('href')), '_system', 'location=yes');
            return false;
        });

        extcal = window.plugins.calendar;

        if (appSpecific.appType === 'mobile') {
            document.addEventListener("online", this.checkConn, false);
            document.addEventListener("offline", this.checkConn, false);
            this.checkConn();
        } else
            conn = true;

        if (appSpecific.userTypes && appSpecific.userTypes.required === true)
            this.setUserType();
        else {
            window.localStorage.setItem("usertype", 1);
            userType = window.localStorage.getItem("usertype") ? window.localStorage.getItem("usertype") : App.init();
        }

        if (appSpecific.validation && appSpecific.validation.required === true) {
            this.validate();
        }
        if (typeof appSpecific.postSplashscreen !== 'undefined' && appSpecific.postSplashscreen.required === true) {
            App.postSplashscreen();
            return false;
        }
        eventsSync.init();
    },
    checkConn: function() {
        var networkState = navigator.connection.type;

        conn = ((networkState === Connection.NONE || networkState === Connection.UNKNOWN || networkState === '') ? false : true);
        return ((conn) ? conn : false);
    },
    setUserType: function() {
        var message = appSpecific.userTypes.question;
        var buttonLabels = appSpecific.userTypes.types[0].buttonLabel + ',' + appSpecific.userTypes.types[1].buttonLabel;

        userType = window.localStorage.getItem("usertype") ? window.localStorage.getItem("usertype") : false;

        if (!userType || userType === false) {
            navigator.notification.confirm(message, this.discernUserType, __('Pregunta'), buttonLabels);
            w = setTimeout(wait, 100);
        }
        clearTimeout(w);
        return true;

    },
    discernUserType: function(index) {
        if (index) {
            window.localStorage.setItem("usertype", index);
            clearTimeout(w);
        } else {
            App.init();
        }

        userType = window.localStorage.getItem("usertype") ? window.localStorage.getItem("usertype") : App.init();
        App.init();
    },
    validate: function() {
        var precond;
        eval(appSpecific.validation.eval);

        if ((precond && precond === true) && window.localStorage.getItem("validatedUser") !== '1') {
            App.validationForm();
        } else {
            return true;
        }
    },
    validationForm: function() {
        var html = '<div id="container-validation">';
        html += '<p>' + __('Por favor, ingrese usuario y contraseña para continuar.') + '</p>';
        html += '<form id="validation" method="get" action=""><input type="text" name="user" id="user" placeholder="' + __('Usuario') + '" /><br /><input type="password" name="password" id="password" placeholder="' + __('Contraseña') + '" /><br /><br /><div class=""><input type="submit" value="' + __('Ingresar') + '" /></div></form>';
        html += '</div>';
        $('body').append(html);
        $('form#validation').submit(function(e) {
            e.preventDefault();

            if ($('#user').val() === '' || $('#password').val() === '') {
                navigator.notification.alert(__('Por favor, ingrese usuario y contraseña'), null, __('Error'));
                return false;
            }

            if (appSpecific.validation.type == 'local') {
                if ($('#user').val() === appSpecific.validation.user && $('#password').val() === appSpecific.validation.password) {
                    window.localStorage.setItem("validatedUser", '1');
                    clearTimeout(w);
                    $('body').html('');
                    App.init();
                } else {
                    navigator.notification.alert(__('Usuario o contraseña incorrectos'), null, __('Error'));
                    return false;
                }
            } else {
                if (conn !== false) {
                    $.get(appSpecific.validation.url, {
                        key: appSpecific.validation.key,
                        email: $('#user').val(),
                        password: $('#password').val()
                    }, function(data, textStatus, jqXHR) {

                        if (parseInt(data) === parseInt(appSpecific.validation.expected)) {
                            window.localStorage.setItem("validatedUser", '1');
                            clearTimeout(w);
                            $('body').html('');
                            window.location.reload();
                        } else {
                            navigator.notification.alert(__('Usuario o contraseña incorrectos'), null, __('Error'));
                            return false;
                        }
                    }, 'html').fail(function() {
                        navigator.notification.alert(__('Problema de conexión. Por favor, inténtelo de nuevo en unos segundos'), null, __('Error'));
                        return false;
                    });
                } else {
                    navigator.notification.alert(__('Para verificar usuario y contraseña debe tener al menos una conexión a internet activa'), null, __('Error'));
                    return false;
                }
            }
        });
        w = setTimeout(wait, 100);
    },
    postSplashscreen: function() {
        var html = '<div id="post-splashscreen">';
        if (appSpecific.postSplashscreen.closeButton && appSpecific.postSplashscreen.closeButton === true) {
            html += '<a href="#" id="post-splashscreen-close-button" onclick="clearTimeout(w); $(\'body\').html(\'\');eventsSync.init();"></a>';
        }
        if (appSpecific.postSplashscreen.url && appSpecific.postSplashscreen.url !== '') {
            html += '<a href="' + appSpecific.postSplashscreen.url + '" target="_system" rel="external">&nbsp;</a>';
        }
        html += '</div>';
        $('body').append(html);
        if (appSpecific.postSplashscreen.closeButton && appSpecific.postSplashscreen.closeButton === true) {
            w = setTimeout(wait, 100);
        } else {
            setTimeout(function() {
                $('body').html('');
                eventsSync.init();
            }, appSpecific.postSplashscreen.delay + ( device.platform ===  'iOS' ? 0 : 10000 - 4000));
        }
    },
    wait: function() {
        return;
    }
}

function dhxReady() {

    jQuery.getScript('./js/lib/dhtmlx/mobile.js', function() {
        jQuery.getScript('./js/lib/dhtmlx/dayevents.js', function() {
            doTheMandinga();
        });
    });
}

function doTheMandinga() {
    scheduler.config.update_render = true;
    if (!data) {
        eventsSync.displayDocs();
        return false;
    }

    dhx.ready(function() {

        dhx.ui.fullScreen();
        dhx.ui({
            view: "scheduler",
            id: "scheduler",
        });

        data = JSON.stringify(data);

        $$("scheduler").parse(data);

        /* preselects Month view*/
        $$("scheduler").$$("buttons").setValue("month");

    });
}

function showLocation() {
    $$("scheduler").$$("locationView").show();

    $$("scheduler").$$("locationView").resize();

    var map = $$("scheduler").$$("mymap").map;
    map.setZoom(12);

    var eventId = $$("scheduler").getCursor();
    var item = $$("scheduler").item(eventId);
    var y = parseFloat(item.y);
    var x = parseFloat(item.x);
    var details = item.details;

    var point = new google.maps.LatLng(x, y);

    map.setCenter(point);

    marker.setMap(map);
}

function doGetNotes(token) {
    return window.localStorage.getItem("notes-" + token) ? window.localStorage.getItem("notes-" + token) : '';
}

function doSaveNotes(token, notes) {
    if (notes.length) window.localStorage.setItem("notes-" + token, notes);
    $$('scheduler').callEvent("onAfterCursorChange", [$$('scheduler').getCursor()]);
    $$('scheduler').$$("views").back();
}

function doDeleteNotes(token) {
    window.localStorage.removeItem("notes-" + token);
    $$('scheduler').callEvent("onAfterCursorChange", [$$('scheduler').getCursor()]);
    $$('scheduler').$$("views").back();
}

function notesForm() {
    $$("scheduler").$$("notesForm").show();
    var eventId = $$("scheduler").getCursor();
    var item = $$("scheduler").item(eventId);
    jQuery('#notes_text textarea').val(doGetNotes(item.token))
    jQuery("#notes_title").html(__('Notas personales sobre: <strong>%1</strong>', item.text));
    jQuery('#item-token').val(item.token);
}

function saveNotes() {
    var notes = jQuery('#notes_text textarea').val();
    var token = jQuery('#item-token').val();
    if (notes !== '') {
        doSaveNotes(token, notes);
    } else {
        if (doGetNotes(token) && doGetNotes(token) !== '') {
            navigator.notification.confirm(__('¿Seguro que desea borrar sus notas para este evento?\nEsta operación no puede ser deshecha.'), function() {
                doDeleteNotes(token);
            });
        } else {
            $$('scheduler').callEvent("onAfterCursorChange", [$$('scheduler').getCursor()]);
            $$('scheduler').$$("views").back();
        }
    }
}

function CalAddSuccessCallback() {
    navigator.notification.alert(__('El evento ha sido agregado al calendario del dispositivo'), null, __('Información'));
}

function errorCallback(e) {}

function addCalendarEvent(title, location, notes, start, end) {
    var startDate = new Date(start);
    var startDate2 = new Date(startDate.getFullYear().toString(), startDate.getMonth().toString(), startDate.getDate().toString(), 0, 0, 0, 0, 0);
    var endDate = new Date(end);
    var endDate2 = new Date(endDate.getFullYear().toString(), endDate.getMonth().toString(), endDate.getDate().toString(), 0, 0, 0, 0, 0);
    endDate2.setDate(endDate2.getDate() + 1);
    extcal.createEvent(title, location, notes, startDate2, endDate2, CalAddSuccessCallback, errorCallback);
}

function showHelp() {
    navigator.notification.alert(appSpecific.helpText, null, __('Información'));
}

function shakeMe(what) {
    $$("scheduler").$$(what).scrollTo(0, 50);
    setTimeout(function() {
        $$("scheduler").$$(what).scrollTo(0, 0);
    }, 200);
    setTimeout(function() {
        $$("scheduler").$$(what).scrollTo(0, 150);
    }, 400);
    setTimeout(function() {
        $$("scheduler").$$(what).scrollTo(0, 0);
    }, 600);
}
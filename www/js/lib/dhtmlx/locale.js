dhx.Date.Locale = {
    month_full:[__("Enero"), __("Febrero"), __("Marzo"), __("Abril"), __("Mayo"), __("Junio"), __("Julio"), __("Agosto"), __("Septiembre"), __("Octubre"), __("Noviembre"), __("Diciembre")],
    month_short:[__("Ene"), __("Feb"), __("Mar*"), __("Abr"), __("May"), __("Jun"), __("Jul"), __("Ago"), __("Sep"), __("Oct"), __("Nov"), __("Dic")],
    day_full:[__("Domingo"), __("Lunes"), __("Martes"), __("Miércoles"), __("Jueves"), __("Viernes"), __("Sábado")],
    day_short:[__("Dom"), __("Lun"), __("Mar"), __("Mie"), __("Jue"), __("Vie"), __("Sab")]
}


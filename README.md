#EC-Calendar

Seguir las instucciones normales hasta la página 6 (edición de los archivos config.json y config.xml).

Luego ejecutar la siguiente línea en un terminal en el directorio raiz del proyecto.

	cordova plugin add cordova-plugin-console cordova-plugin-device cordova-plugin-dialogs cordova-plugin-globalization cordova-plugin-inappbrowser cordova-plugin-network-information cordova-plugin-splashscreen cordova-plugin-statusbar cordova-plugin-vibration cordova-plugin-whitelist ./nl.x-services.plugins.calendar
	
Finalmente, continuar con las instrucciones normales desde el "SEGUIMOS DESDE ACA" de la página 7 ("Modificaciones de idioma default...").

En caso de necesitar borrar los plugins una vez instalados para luego volver a instalarlos o lo que haga falta:

	cordova plugin rm cordova-plugin-console cordova-plugin-device cordova-plugin-dialogs cordova-plugin-globalization cordova-plugin-inappbrowser cordova-plugin-network-information cordova-plugin-splashscreen cordova-plugin-statusbar cordova-plugin-vibration cordova-plugin-whitelist nl.x-services.plugins.calendar